reddit User Profile Redirector
==============================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a user's profile page on reddit -- will skip its new design and will redirect to the legacy version (e.g., `https://www.reddit.com/user/USERNAME/overview`).
