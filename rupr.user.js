// ==UserScript==
// @name        reddit user profile redirector
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-reddit-user-profile-redirector
// @downloadURL https://bitbucket.org/dsjkvf/userscript-reddit-user-profile-redirector/raw/master/rupr.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-reddit-user-profile-redirector/raw/master/rupr.user.js
// @include     https://*.reddit.com/user/*
// @run-at      document-start
// @version     1.01
// @grant       none
// ==/UserScript==

var oldUrl  = window.location.pathname;

// Add a closing '/' if needed

if ( ! /\/$/.test (oldUrl) ) {

    var oldUrl = oldUrl + '/'
}

// Add a closing 'overview' if needed

if ( ! /overview\/$/.test (oldUrl) ) {

    var newURL  = window.location.protocol + '//'
                + window.location.host
                + oldUrl + 'overview/'
                ;

// replace() puts the good page in the history instead of the bad page.

    window.location.replace(newURL);
}
